using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class ScripUIPlay : MonoBehaviour
{
    public Slider sliderMusica;
    public AudioMixer mixer;
    public GameObject menudepausa;
    //public float valor;
    private string volumen = "MV";

    public Slider slider;
    public int maximoVida;
    public int vidaActual;



    public void ponerMaximoVida(int maximoVida)
    {
        slider.maxValue = maximoVida;
        slider.value = maximoVida;
    }

    public void ponerSalud(int vidaActual)
    {
        slider.value = vidaActual;
    }


    void Start()
    {
        
    }

    public void Pause()
    {
        menudepausa.SetActive(true);
    }

    public void DesPause()
    {
        menudepausa.SetActive(false);
    }

    public void Menudeinicio()
    {
        SceneManager.LoadScene("menu_de_inicio");
    }

    public void cloushGame()
    {
        Application.Quit();
        Debug.Log("Game is exiting");
    }

    public void Startgame()
    {
        SceneManager.LoadScene("Game");
    }

    public void Setlevel(float valor)
    {
        mixer.SetFloat(volumen, Mathf.Log10(valor ) * 30f);
    }

}
