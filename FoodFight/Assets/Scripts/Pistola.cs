using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Pistola : MonoBehaviour
{

    public GameObject bala;
    public Rigidbody rb;
    private bool cadencia = false;
    public AudioSource disparo;

    public void disparar()
    {
        
        if (cadencia == false)
        {
            StartCoroutine(espera());
        }

        
        IEnumerator espera()
        {
            cadencia = true;
            disparo.Play(0);
            Rigidbody rb = Instantiate(bala, transform.position, Quaternion.identity).GetComponent<Rigidbody>();
            rb.AddForce(transform.forward * 20f, ForceMode.Impulse);
            yield return new WaitForSeconds(1f);
            cadencia = false;
        }

    }
}
