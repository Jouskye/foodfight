using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScriptBala : MonoBehaviour
{
    public int damage = 1;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Enemy")
        {
            other.GetComponent<EnemyScript>().matar(damage);
            Destroy(gameObject);
        }
        if (other.tag == "Techo")
        {
            Destroy(gameObject);
        }
        if (other.tag == "Boss")
        {
            other.GetComponent<BossScript>().danyar(damage);
            Destroy(gameObject);
        }


    }

}
