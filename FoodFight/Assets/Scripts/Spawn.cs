using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform[] spawnPoints;
    public GameObject[] enemigos;
    public GameObject enemigoBoss;
    public Animator animator;
    public GameObject pantallaPerdiste;

    public int tiempo;


    public void Start()
    {
        
    }


    public void escanea()
    {
        tiempo = 7;
        StartCoroutine(StartSpawning());
        StartCoroutine(SiguienteOrda());
        StartCoroutine(Boss());
    }


   IEnumerator StartSpawning()
    {

        yield return new WaitForSeconds(tiempo);

        int n = Random.Range(0, enemigos.Length);
        int a = Random.Range(0, spawnPoints.Length);

        Instantiate(enemigos[n], spawnPoints[a].position, spawnPoints[a].transform.rotation);

        StartCoroutine(StartSpawning());

    }


    IEnumerator SiguienteOrda()
    {

        yield return new WaitForSeconds(20);
        tiempo = 3;

    }


    IEnumerator Boss()
    {

        yield return new WaitForSeconds(50);
        tiempo = 900;
        yield return new WaitForSeconds(10);
        boss();
        yield return new WaitForSeconds(24);
        pantallaPerdiste.SetActive(true);
    }

    private void boss()
    {
        enemigoBoss.SetActive(true);
        animator.Play("RecoridoBoss");
        

    }

}
