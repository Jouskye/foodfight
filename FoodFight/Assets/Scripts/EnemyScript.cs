using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{

    public Rigidbody rb;
    public Transform player;
    public int puntos = 10;
   
    
    public int damage = 1;

    public int fuerza;

    public int vida;

    

    // Update is called once per frame
    void Update()
    {
        
        transform.LookAt(player);
        transform.Translate(Vector3.up * Time.fixedDeltaTime * 0.2f);
        StartCoroutine(muevete());
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Person")
        {
            other.GetComponent<PlayerScript>().restarVida(damage);
            Destroy(gameObject);
        }
        else if (other.tag == "Techo")
        {
            Destroy(gameObject);
        }

    }


    public void matar(int damage)
    {
        vida -= damage;

        if (vida <= 0)
        {       
            Destroy(gameObject);


        }

    }



    IEnumerator muevete()
    {

        yield return new WaitForSeconds(9f);
        rb.AddForce(transform.forward * fuerza);
    }

}
