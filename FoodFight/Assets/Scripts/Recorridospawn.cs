using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Recorridospawn : MonoBehaviour
{
    // Start is called before the first frame update
    private Transform puntoOrigen;
    private Transform puntoDestino;

    //Array de puntos

    public Transform[] puntosRecorrido;

    private int indiceOrigen = 0;

    public float velocidad;

    void Start()
    {

        puntoOrigen = puntosRecorrido[0];
        puntoDestino = puntosRecorrido[1];
        transform.position = puntoOrigen.position;

    }


    void Update()
    {
        deaquialla();
    }

    private void deaquialla()
    {

        //Realizar movimineto
        //Verificar transiciones
        //Si transicion -> cambio estado y accion inicial
        //Ida, ida, ida

        float distanciaEntrePuntos = Vector3.Distance(puntoOrigen.position, puntoDestino.position);
        float distanciaRecorrida = Vector3.Distance(puntoOrigen.position, transform.position);

        distanciaRecorrida = distanciaRecorrida + velocidad * Time.deltaTime;
        transform.position = Vector3.Lerp(puntoOrigen.position, puntoDestino.position, distanciaRecorrida / distanciaEntrePuntos);

        if (Vector3.Distance(puntoDestino.position, transform.position) < 0.5f)
        {

            indiceOrigen = indiceOrigen + 1;
            indiceOrigen = indiceOrigen % puntosRecorrido.Length;
            puntoOrigen = puntosRecorrido[indiceOrigen];
            puntoDestino = puntosRecorrido[(indiceOrigen + 1) % puntosRecorrido.Length];

        }
    }
}
