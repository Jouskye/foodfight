using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour
{
    // Start is called before the first frame update

    public int maximoVida = 10;
    public int vidaActual;
    public ScripUIPlay gameManager;
    public GameObject pantallaPerdiste;

    void Start()
    {
        vidaActual = maximoVida;

        gameManager.ponerMaximoVida(maximoVida);
    }


    public void restarVida(int damage)
    {
        vidaActual -= damage;
        gameManager.ponerSalud(vidaActual);


        if (vidaActual <= 0)
        {

            pantallaPerdiste.SetActive(true);
            Destroy(gameObject);

        }

    }

}
