using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
        public int Score = 0;
        public static GameManager Instance { get; private set; }

        void Awake()
        {
            if (Instance == null) { Instance = this; } else if (Instance != this) { Destroy(this); }
            DontDestroyOnLoad(gameObject);
        }



}
