using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ScoreBoard : MonoBehaviour
{
    public Text text;
    public int totalscore = 00;

    // Start is called before the first frame update
    void Start()
    {
        text.text = "" + totalscore;
    }


    public void Ganaspuntuacion(int puntos)
    {
        totalscore += puntos;
        text.text = "" + totalscore;
    }

    // Update is called once per frame

}
